module Web
  module Controllers
    module Users
      class Create
        include Web::Action

        params do
          required(:user).schema do
            required(:name).filled(:str?)
            required(:email).filled(:str?)
          end
        end

        def call(params)
          if params.valid?
            
            @user = UserRepository.new.create(params[:user])

           @user = UserRepository.new.update(@user.id ,verified: "null")

           
           self.body = "User Created"
           # redirect_to '/users'
          end
        end
      end
    end
  end
end
