module Web
  module Controllers
    module Users
      class Update
        include Web::Action
        

        def call(params)
          @find = UserRepository.new.find(params[:id])
          @users = UserRepository.new.update(@find.id , name: params[:name])
          
          @id=@users.id
          @name=@users.name
          @contact=@users.contact
          @email=@users.email
          self.body = "id :#@id name :#@name contact :#@contact email :#@email"
        end

      end
    end
  end
end
