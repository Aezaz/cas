module Web
  module Controllers
    module Users
      class Find
        include Web::Action

        def call(params)

          @users = UserRepository.new.find(params[:id])
          
          @id=@users.id
          @name=@users.name
          @contact=@users.contact
          @email=@users.email
          self.body = "id :#@id name :#@name contact :#@contact email :#@email"

        end
      end
    end
  end
end
