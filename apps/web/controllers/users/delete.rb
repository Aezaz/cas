module Web
  module Controllers
    module Users
      class Delete
        include Web::Action

        def call(params)
          @users = UserRepository.new.delete(params[:id])

          self.body = "User deleted"
        end
      end
    end
  end
end
