# Configure your routes here
# See: http://hanamirb.org/guides/routing/overview/
#
# Example:
# get '/hello', to: ->(env) { [200, {}, ['Hello from Hanami!']] }
root to: 'home#index'
get '/users', to: 'users#index'
get '/users/new', to: 'users#new'
post '/users', to: 'users#create'
get '/users/find', to: 'users#find'
post '/users/update', to: 'users#update'
get '/users/delete', to: 'users#delete'
get '/users/otp', to: 'users#otp'
post '/users/contact', to: 'users#contact'
