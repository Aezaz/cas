Hanami::Model.migration do
  change do
    create_table :users do
      primary_key :id
      column :name, String, null: false
      column :contact, String, null: true, unique: true
      column :email, String, null: false, unique: true
      column :otp, String, null: true
      column :verified, String, null: true
      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end
  end
end
